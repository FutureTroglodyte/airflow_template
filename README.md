# Airflow Template Project


Using Apache Airflow version 2.5.0 with Python version 3.7.15 (latest in 2023-01-14) under Ubuntu 22.04.1 LTS.


## HowTos


### Setup the "Local" Development Environment

```sh
[...]

$ pre-commit install
```

### Add Custom apt/pip Packages

Just edit the Dockerfile as described here: https://airflow.apache.org/docs/docker-stack/build.html


### Update the airflow-default-base-Dockerfile

Find the latest version here: https://github.com/apache/airflow/blob/main/Dockerfile


### Update & Deploy the airflow.cfg

**Update:**
```sh
$ docker compose up -d
$ docker cp airflow_template-airflow-worker-1:/opt/airflow/airflow.cfg /docs/airflow-default.cfg
```

**Deplos:** Try https://stackoverflow.com/questions/55347077/

### Update Connections & Variable by CLI Script

Write a bash script with commands like:
```sh
#!/bin/bash

### Update airflow connections ###

bash airflow.sh connections delete <conn-id>
bash airflow.sh connections import config/airflow_connections.yaml

### Update airflow variables ###

bash airflow.sh variables import config/airflow_variables.json
```

### Create a Local Development conda-environment.yml


Run:
``` sh
### Create an empty conda environment ###

$ conda create -n airflow-template python==3.7.15
$ conda activate airflow-template

### Install the packages via pip ###

# Solve mysqlclient issues
$ sudo apt-get install libmysqlclient-dev

# Solve psycopg2 issues
$ sudo apt install python3-dev libpq-dev

# Solve python-ldap issues
$ apt-get install build-essential python3-dev python2.7-dev \
    libldap2-dev libsasl2-dev slapd ldap-utils tox \
    lcov valgrind

# Install all pip-packages
$ pip install -r docs/airflow-default-pip-requirements.txt

### Install additional packages ###
# Hint: Always check for conflicts with & changes of other pip packages!

$ conda install pre-commit ipykernel

### Export conda environment

$ conda env export > conda-environment.yml
```

#### Issues

**FIXME:** `$ pip install mysqlclient==2.1.1` does not work, throwing
``` sh
Preparing metadata (setup.py) ... error
error: subprocess-exited-with-error

× python setup.py egg_info did not run successfully.
│ exit code: 1
╰─> [16 lines of output]
    /bin/sh: 1: mysql_config: not found
    /bin/sh: 1: mariadb_config: not found
    /bin/sh: 1: mysql_config: not found

[...]
```
Probable Solution: https://stackoverflow.com/questions/5178292/

**FIXME:** `$ pip install psycopg2==2.9.5` does not work, throwing
``` sh
Preparing metadata (setup.py) ... error
error: subprocess-exited-with-error

× python setup.py egg_info did not run successfully.
│ exit code: 1
╰─> [25 lines of output]
    running egg_info
    creating /tmp/pip-pip-egg-info-mrl4g0ba/psycopg2.egg-info
    writing /tmp/pip-pip-egg-info-mrl4g0ba/psycopg2.egg-info/PKG-INFO
    writing dependency_links to /tmp/pip-pip-egg-info-mrl4g0ba/psycopg2.egg-info/dependency_links.txt
    writing top-level names to /tmp/pip-pip-egg-info-mrl4g0ba/psycopg2.egg-info/top_level.txt
    writing manifest file '/tmp/pip-pip-egg-info-mrl4g0ba/psycopg2.egg-info/SOURCES.txt'
    /home/ft/miniconda3/envs/airflow-template/lib/python3.7/site-packages/setuptools/config/setupcfg.py:508: SetuptoolsDeprecationWarning: The license_file parameter is deprecated, use license_files instead.
      warnings.warn(msg, warning_class)

      Error: pg_config executable not found.

[...]
```
Probable Solution: https://stackoverflow.com/questions/61528483/

**FIXME:** `Building wheels for collected packages` fails for `python-ldap`, throwing
``` sh
error: subprocess-exited-with-error

× Building wheel for python-ldap (pyproject.toml) did not run successfully.

[...]

gcc -pthread -B /home/ft/miniconda3/envs/airflow-template/compiler_compat -Wl,--sysroot=/ -Wsign-compare -DNDEBUG -g -fwrapv -O3 -Wall -Wstrict-prototypes -fPIC -DHAVE_SASL -DHAVE_TLS -DLDAPMODULE_VERSION=3.4.3 "-DLDAPMODULE_AUTHOR=python-ldap project" "-DLDAPMODULE_LICENSE=Python style" -IModules -I/home/ft/miniconda3/envs/airflow-template/include/python3.7m -c Modules/LDAPObject.c -o build/temp.linux-x86_64-cpython-37/Modules/LDAPObject.o
      In file included from Modules/LDAPObject.c:3:
      Modules/common.h:15:10: fatal error: lber.h: No such file or directory
         15 | #include <lber.h>
            |          ^~~~~~~~
      compilation terminated.
      error: command '/usr/bin/gcc' failed with exit code 1
      [end of output]

```
Probable Solution: https://www.python-ldap.org/en/python-ldap-3.3.0/installing.html#debian


## Initialize, Run and Stop Airflow

For the first time, initialize airflow with `$ docker compose up airflow-init`.

You can run airflow with `$ docker compose up -d`. It will then autostart when booting. To stop airflow or prevent autostarting run `$ docker compose down`.


## Initial Setup Instructions

- Install *docker*: https://docs.docker.com/engine/install/ubuntu/
- Manage *docker* as a non-root user: https://docs.docker.com/engine/install/linux-postinstall/#manage-docker-as-a-non-root-user
- Install *docker compose* [sic!]: https://docs.docker.com/compose/install/linux/#install-using-the-repository

### Base Docker Compose Configuration

- Get it from here: https://airflow.apache.org/docs/apache-airflow/stable/howto/docker-compose/index.html


## TODOs

- [x] Add a custom Dockerfile template
- [x] Copy of Dockerfile's base layer in `docs/`
- [ ] Add a real example DAG, with example Tasks
- [x] Add the official *airflow.sh* CLI entrypoint
- [x] Add the default airflow config file
- [x] Find a way to update the airflow config in the containers
- [ ] HowTo: Set up the "local" development environment
- [x] Create a basic conda-environment.yaml file for the "local" development environment
  - [x] What Python Version is used in the airflow docker containers?
  - [x] Solve issues when installing *airflow default pip requirements*  
  - [x] What additional packages are needed?
    - [x] pre-commit + yaml file
    - [x] jupyter stuff?
    - [x] plotting stuff? Not necessary!
- [ ] Add some HowTos
- [ ] Restructure this *README.md*
- [ ] Remove TODOs, when all are done
