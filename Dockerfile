FROM apache/airflow:2.5.0

### Install additional apt packages (placeholder <apt-package>) ###

# USER root

# RUN apt-get update \
#     && apt-get install -y --no-install-recommends \
#         <apt-package> \
#         <apt-package> \
#     && apt-get autoremove -yqq --purge \
#     && apt-get clean \
#     && rm -rf /var/lib/apt/lists/*

# USER airflow

### Install additional pip packages (placeholder <pip-package>==<version>) ###

# RUN pip install --no-cache-dir \
#     <pip-package>==<version> \
#     <pip-package>==<version>
